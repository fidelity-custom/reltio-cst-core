package com.reltio.cst.service;

import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;

/**
 * This interface provides way to get the token for the given user credentials
 */
public interface TokenGeneratorService {

    @Deprecated
    boolean startBackgroundTokenGenerator();

    String getToken() throws APICallFailureException, GenericException;

    String getNewToken() throws APICallFailureException,
            GenericException;

    @Deprecated
    boolean stopBackgroundTokenGenerator();

    String getRefreshToken() throws APICallFailureException,
            GenericException;

}
