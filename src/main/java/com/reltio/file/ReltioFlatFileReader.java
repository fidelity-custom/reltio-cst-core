package com.reltio.file;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.tika.parser.txt.CharsetDetector;
import org.apache.tika.parser.txt.CharsetMatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;

public class ReltioFlatFileReader implements ReltioFileReader, AutoCloseable {

    private static Logger logger = LoggerFactory.getLogger(ReltioFlatFileReader.class.getName());
    private final CSVReader csvReader;

    public ReltioFlatFileReader(String fileName, String separator, char quoteChar, char escapeChar) throws Exception {
        CharsetMatch cm = getCharsetMatch(fileName);
        this.csvReader = getFileReader(fileName, cm, separator, quoteChar, escapeChar);
    }

    public ReltioFlatFileReader(String fileName, char quoteChar, char escapeChar) throws IOException {
        CharsetMatch cm = getCharsetMatch(fileName);
        String separator = null;
        this.csvReader = getFileReader(fileName, cm, separator, quoteChar, escapeChar);
    }

    public ReltioFlatFileReader(String fileName, String separator, String decoder, char quoteChar, char escapeChar) throws IOException {
    	char csvSeparator;
    	BufferedReader fileReader = null;
        CharsetDecoder newDecoder = Charset.forName(decoder).newDecoder();
        newDecoder.onMalformedInput(CodingErrorAction.REPLACE);
        fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(StringEscapeUtils.escapeJava(fileName)), newDecoder));
        // BOM marker will only appear on the very beginning
        skipBOMMarker(fileReader);
        if (separator != null) {
        	csvSeparator=separator.charAt(0);
        } else {
        	csvSeparator = '|';
        }
        //this.csvReader = new CSVReader(fileReader,csvSeparator);
        CSVParser parser = new CSVParserBuilder().withSeparator(csvSeparator).withEscapeChar(escapeChar).withQuoteChar(quoteChar).build();
        this.csvReader = new CSVReaderBuilder(fileReader).withCSVParser(parser).build();
    }

    private CSVReader getFileReader(String fileName, CharsetMatch cm, String separator, char quoteChar, char escapeChar) throws FileNotFoundException {
    	char csvSeparator;
    	BufferedReader fileReader = null;
        if (Charset.availableCharsets().get(cm.getName()) == null
                || cm.getName().equals("Shift_JIS")) {
            logger.info("The "
                    + cm.getName()
                    + " charset not supported. So letting the reader to choose apporiate the Charset...");
            fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(StringEscapeUtils.escapeJava(fileName))));
        } else {
            CharsetDecoder newdecoder = Charset.forName(cm.getName()).newDecoder();
            newdecoder.onMalformedInput(CodingErrorAction.REPLACE);

            fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(StringEscapeUtils.escapeJava(fileName)), newdecoder));
        }
        
        skipBOMMarker(fileReader);
        if (separator != null) {
        	csvSeparator=separator.charAt(0);
        } else {
        	csvSeparator = '|';
        }
        //quoteChar - DEFAULT_QUOTE_CHARACTER for my readers, NO_QUOTE_CHARACTER for Karthik
        //escapeChar - NO_ESCAPE_CHARACTER for my readers, backslash for Karthik
        CSVParser parser = new CSVParserBuilder().withSeparator(csvSeparator).withEscapeChar(escapeChar).withQuoteChar(quoteChar).build();
        return new CSVReaderBuilder(fileReader).withCSVParser(parser).build();
    }

    private CharsetMatch getCharsetMatch(String fileName) throws IOException {
        CharsetMatch cm;
        try (BufferedInputStream isr = new BufferedInputStream(new FileInputStream(StringEscapeUtils.escapeJava(fileName)))) {
            CharsetDetector charsetDetector = new CharsetDetector();
            charsetDetector.setText(isr);
            charsetDetector.enableInputFilter(true);
            cm = charsetDetector.detect();
            logger.info("Decorder of the" + fileName + "(CharSet) :: " + cm.getName());
        }
        return cm;
    }

    @Override
    public String[] readLine() throws IOException {
    	String[] newLineValues = null;
        try {
        	newLineValues = csvReader.readNext();
		} catch (CsvValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return newLineValues;
    }

    @Override
    public void close() throws IOException {
        csvReader.close();
    }

    private void skipBOMMarker(BufferedReader fileReader) {
        try {
            fileReader.mark(4);
            if ('\ufeff' != fileReader.read())
                fileReader.reset(); // not the BOM marker
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
