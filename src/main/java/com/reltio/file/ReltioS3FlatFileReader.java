package com.reltio.file;

import org.apache.tika.parser.txt.CharsetDetector;
import org.apache.tika.parser.txt.CharsetMatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.reltio.cst.util.AWSS3Util;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;
import com.amazonaws.AmazonServiceException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;

/**
 * @author Mallikarjuna.Aakulati@reltio.com Created : May 28, 2021
 */
public class ReltioS3FlatFileReader implements ReltioFileReader, AutoCloseable {

    private static Logger logger = LoggerFactory.getLogger(ReltioS3FlatFileReader.class.getName());
    private final CSVReader csvReader;
    private S3Object s3object = null;
    Exception caughtException = null;

    public ReltioS3FlatFileReader(String awsKey,String awsSecretKey, String awsRegion, String bucket, String fileName, String separator, char quoteChar, char escapeChar) throws Exception {
        CharsetMatch cm = getCharsetMatch(awsKey, awsSecretKey, awsRegion, bucket, fileName);
        this.csvReader = getFileReader(awsKey, awsSecretKey, awsRegion, bucket, fileName, cm, separator, quoteChar, escapeChar);
    }

    public ReltioS3FlatFileReader(String awsKey,String awsSecretKey, String awsRegion, String bucket, String fileName, char quoteChar, char escapeChar) throws IOException {
        CharsetMatch cm = getCharsetMatch(awsKey, awsSecretKey, awsRegion, bucket, fileName);
        String separator = null;
        this.csvReader = getFileReader(awsKey, awsSecretKey, awsRegion, bucket, fileName, cm, separator, quoteChar, escapeChar);
    }

    public ReltioS3FlatFileReader(String awsKey,String awsSecretKey, String awsRegion, String bucket, String fileName, String separator, String decoder, char quoteChar, char escapeChar) throws IOException {
    	BufferedReader fileReader = null;
        char csvSeparator;
        CharsetDecoder newDecoder = Charset.forName(decoder).newDecoder();
        newDecoder.onMalformedInput(CodingErrorAction.REPLACE);
        try {
        AmazonS3 s3Client = null;
        s3Client=AWSS3Util.getS3Client(awsKey, awsSecretKey, awsRegion);
    	s3object = s3Client.getObject(bucket, fileName);
        }catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process 
            // it, so it returned an error response.
        	caughtException = e;
    		logger.error("Amazon S3 Flat File Service Exception :: ");
			logger.error(e.getMessage());
            e.printStackTrace();
        }catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
        	caughtException = e;
        	logger.error("Amazon S3 Flat File SDK Client exception :: ");
			logger.error(e.getMessage());
            e.printStackTrace();
        }finally {
            // To ensure that the network connection doesn't remain open, close any open input streams.
        	if (caughtException != null) {
            	logger.error("Exeption in Reltio S3 CSV Read Program :: ");
    			logger.error(caughtException.getMessage());
            	}
        }
        fileReader = new BufferedReader(new InputStreamReader(s3object.getObjectContent(), newDecoder));
        // BOM marker will only appear on the very beginning
        skipBOMMarker(fileReader);
        if (separator != null) {
        	csvSeparator=separator.charAt(0);
        } else {
        	csvSeparator = '|';
        }
//        this.csvReader = new CSVReader(fileReader,csvSeparator);
        CSVParser parser = new CSVParserBuilder().withSeparator(csvSeparator).withEscapeChar(escapeChar).withQuoteChar(quoteChar).build();
        this.csvReader = new CSVReaderBuilder(fileReader).withCSVParser(parser).build();
    }

    private CSVReader getFileReader(String awsKey,String awsSecretKey, String awsRegion, String bucket, String fileName, CharsetMatch cm, String separator, char quoteChar, char escapeChar) throws FileNotFoundException, IOException {
        BufferedReader fileReader = null;
        char csvSeparator;
        try {
        AmazonS3 s3Client = null;
        s3Client=AWSS3Util.getS3Client(awsKey, awsSecretKey, awsRegion);
    	s3object = s3Client.getObject(bucket, fileName);
        if (Charset.availableCharsets().get(cm.getName()) == null
                || cm.getName().equals("Shift_JIS")) {
            logger.info("The "
                    + cm.getName()
                    + " charset not supported. So letting the reader to choose apporiate the Charset...");
            fileReader = new BufferedReader(new InputStreamReader(s3object.getObjectContent()));
        } else {
            CharsetDecoder newdecoder = Charset.forName(cm.getName()).newDecoder();
            newdecoder.onMalformedInput(CodingErrorAction.REPLACE);

            fileReader = new BufferedReader(new InputStreamReader(s3object.getObjectContent(), newdecoder));
        }
        }catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process 
            // it, so it returned an error response.
        	caughtException = e;
    		logger.error("Amazon S3 Flat File Service Exception :: ");
			logger.error(e.getMessage());
            e.printStackTrace();
        }catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
        	caughtException = e;
        	logger.error("Amazon S3 Flat File SDK Client exception :: ");
			logger.error(e.getMessage());
            e.printStackTrace();
        }finally {
            // To ensure that the network connection doesn't remain open, close any open input streams.
        	if (caughtException != null) {
            	logger.error("Exeption in Reltio S3 CSV Read Program :: ");
    			logger.error(caughtException.getMessage());
            	}
        }
        skipBOMMarker(fileReader);
        if (separator != null) {
        	csvSeparator=separator.charAt(0);
        } else {
        	csvSeparator = '|';
        }
        CSVParser parser = new CSVParserBuilder().withSeparator(csvSeparator).withEscapeChar(escapeChar).withQuoteChar(quoteChar).build();
        return new CSVReaderBuilder(fileReader).withCSVParser(parser).build();
    }

    private CharsetMatch getCharsetMatch(String awsKey,String awsSecretKey, String awsRegion, String bucket, String fileName) throws IOException {
        CharsetMatch cm = null;
        try {
        	AmazonS3 s3Client = null;
        	s3Client=AWSS3Util.getS3Client(awsKey, awsSecretKey, awsRegion);
        	s3object = s3Client.getObject(bucket, fileName);
        	BufferedInputStream isr = new BufferedInputStream(s3object.getObjectContent());

            CharsetDetector charsetDetector = new CharsetDetector();
            charsetDetector.setText(isr);
            charsetDetector.enableInputFilter(true);
            cm = charsetDetector.detect();
            isr.close();
            
            logger.info("Decorder of the" + fileName + "(CharSet) :: " + cm.getName());
        }catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process 
            // it, so it returned an error response.
    		logger.error("Amazon S3 Flat File Service Exception :: ");
			logger.error(e.getMessage());
            e.printStackTrace();
        }catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
        	logger.error("Amazon S3 Flat File SDK Client exception :: ");
			logger.error(e.getMessage());
            e.printStackTrace();
        }finally {
            // To ensure that the network connection doesn't remain open, close any open input streams.
            if (s3object != null) {
            	s3object.close();
            }
        }
        return cm;
    }

    @Override
    public String[] readLine() throws IOException {
    	String[] newLineValues = null;
        try {
        	newLineValues = csvReader.readNext();
		} catch (CsvValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return newLineValues;
    }

    @Override
    public void close() throws IOException {
        csvReader.close();
    }

    private void skipBOMMarker(BufferedReader fileReader) {
        try {
            fileReader.mark(4);
            if ('\ufeff' != fileReader.read())
                fileReader.reset(); // not the BOM marker
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
