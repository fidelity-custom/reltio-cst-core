package com.reltio.file;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;
import java.util.List;

/**
 * @author Ganesh.Palanisamy@reltio.com Created : Sep 20, 2014
 */
public class ReltioFlatFileWriter implements ReltioFileWriter, AutoCloseable {
    private static Logger logger = LoggerFactory.getLogger(ReltioFlatFileReader.class.getName());

    private final BufferedWriter bw;
    public static final int INITIAL_STRING_SIZE = 128;
    private static final String lineEnd = System.lineSeparator();

    private String separator = "|";

    private CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();

    /**
     * By default UTF-8 encoding used and separator will be pipe
     *
     * @param fileName
     * @throws IOException
     */
    public ReltioFlatFileWriter(String fileName) throws IOException {
        encoder.onMalformedInput(CodingErrorAction.REPORT);
        encoder.onUnmappableCharacter(CodingErrorAction.REPORT);
        bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
                StringEscapeUtils.escapeJava(fileName)), encoder));
    }

    public ReltioFlatFileWriter(String fileName, String encoding)
            throws IOException {

    	encoder = Charset.forName(encoding).newEncoder();
        encoder.onMalformedInput(CodingErrorAction.REPORT);
        encoder.onUnmappableCharacter(CodingErrorAction.REPORT);

        bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
                StringEscapeUtils.escapeJava(fileName)), encoder));
    }

    public ReltioFlatFileWriter(String fileName, String encoding,
                                String separator) throws IOException {

    	encoder = Charset.forName(encoding).newEncoder();
        encoder.onMalformedInput(CodingErrorAction.REPORT);
        encoder.onUnmappableCharacter(CodingErrorAction.REPORT);

        bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
                StringEscapeUtils.escapeJava(fileName)), encoder));

        this.separator = separator;
    }

    public void writeToFile(List<String[]> lines) throws IOException {
        if (lines != null) {
            synchronized (bw) {
                for (String[] line : lines) {
                    writeDataToFile(line);
                }
                bw.flush();
            }
        }
    }

    public void close() throws IOException {
        synchronized (bw) {
            bw.close();
        }
    }

    /**
     * Writes the next line to the file.
     *
     * @param nextLine a string array with each comma-separated element as a separate
     *                 entry.
     * @throws IOException
     */
    public void writeToFile(String[] nextLine) throws IOException {
        synchronized (bw) {
            writeDataToFile(nextLine);
            bw.flush();
        }
    }

    private void writeDataToFile(String[] nextLine) throws IOException {
        if (nextLine == null)
            return;
        StringBuilder sb = new StringBuilder(INITIAL_STRING_SIZE);
        for (int i = 0; i < nextLine.length; i++) {

            if (i != 0) {
                sb.append(separator);
            }

            String nextElement = nextLine[i];
            if (nextElement == null)
                continue;
            sb.append(nextElement);
        }

        sb.append(lineEnd);
        bw.write(sb.toString());
    }

    @Override
    public void writeBulkToFile(List<String> lines) throws IOException {
        if (lines != null && !lines.isEmpty()) {
            synchronized (bw) {
                for (String line : lines) {
                    bw.write(line + lineEnd);
                }
                bw.flush();
            }
        }
    }

    @Override
    public void writeToFile(String line) throws IOException {
        if (line != null) {
            synchronized (bw) {
                bw.write(line + lineEnd);
            }
        }
    }

    /**
     * @param separator the separator to set
     */
    public void setSeparator(String separator) {
        this.separator = separator;
    }
}
