package com.reltio.file;

import com.opencsv.CSVWriter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.reltio.cst.util.AWSS3Util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Mallikarjuna.Aakulati@reltio.com Created : July 05, 2021
 */
public class ReltioS3RejectFileWriter implements ReltioFileWriter, AutoCloseable {
    private static Logger logger = LoggerFactory.getLogger(ReltioS3RejectFileWriter.class.getName());

    public static final int INITIAL_STRING_SIZE = 128;
    private static final String lineEnd = CSVWriter.DEFAULT_LINE_END;
    private ByteArrayOutputStream stream = new ByteArrayOutputStream();
    private final CSVWriter writer;
    private AmazonS3 s3Client;
    private String bucket;
    private String fileName;
    private String encoding="UTF-8";
    private final Set<String> crosswalkUnique = new HashSet<String>();
    CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();

    public ReltioS3RejectFileWriter(String bucket, String fileName, String awsKey, String awsSecretKey, String awsRegion, String separator) throws IOException {
        encoder.onMalformedInput(CodingErrorAction.REPLACE);
        encoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
        char csvSeparator;
        this.s3Client=AWSS3Util.getS3Client(awsKey, awsSecretKey, awsRegion);
        stream.write(0xef);
        stream.write(0xbb);
        stream.write(0xbf);
        OutputStreamWriter streamWriter = new OutputStreamWriter(stream, encoder);
        if (separator != null) {
        	csvSeparator=separator.charAt(0);
        } else {
        	csvSeparator = '|';
        }
//        writer =  new CSVWriter(streamWriter, csvSeparator);
        writer = new CSVWriter(streamWriter, csvSeparator,
                CSVWriter.DEFAULT_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);
        this.bucket = bucket;
        this.fileName = fileName;
    }
    
    public ReltioS3RejectFileWriter(String bucket, String fileName, String awsKey, String awsSecretKey, String awsRegion, String encoding, String separator)
            throws IOException {

    	this.encoding=encoding;
        encoder = Charset.forName(encoding).newEncoder();
        encoder.onMalformedInput(CodingErrorAction.REPORT);
        encoder.onUnmappableCharacter(CodingErrorAction.REPORT);
        char csvSeparator;
        this.s3Client=AWSS3Util.getS3Client(awsKey, awsSecretKey, awsRegion);
        OutputStreamWriter streamWriter = new OutputStreamWriter(stream, encoder);
        if (separator != null) {
        	csvSeparator=separator.charAt(0);
        } else {
        	csvSeparator = '|';
        }
//        writer =  new CSVWriter(streamWriter, csvSeparator);
        writer = new CSVWriter(streamWriter, csvSeparator,
                CSVWriter.DEFAULT_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);
        this.bucket = bucket;
        this.fileName = fileName;
    }

    public void writeToFile(String[] line) {
    	if (line == null)
            return;
    	synchronized (writer) {
        writer.writeNext(line);
    	}
    }

    public void close() throws IOException {
    	writer.flush();
    	if (stream.size() > 6){
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("text/csv");
            metadata.setContentEncoding(encoding);
            metadata.setContentLength(stream.size());
            writer.close();
            s3Client.putObject(this.bucket, this.fileName, new ByteArrayInputStream(stream.toByteArray()), metadata);
        	}
    }

    @Override
    public void writeBulkToFile(List<String> lines) {
        // TODO Not implemented

    }

    @Override
    public void writeToFile(String line) {
        // TODO Not Implemented

    }

	@Override
	public synchronized void writeToFile(List<String[]> lines) throws IOException {
        if (lines != null) {
        	
        	ByteArrayOutputStream tempStream = new ByteArrayOutputStream();
	        OutputStreamWriter streamWriter = new OutputStreamWriter(tempStream, encoder.charset());
	        CSVWriter tempWriter = new CSVWriter(streamWriter, '|',
	                CSVWriter.DEFAULT_QUOTE_CHARACTER,
	                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
	                CSVWriter.DEFAULT_LINE_END);
	        tempWriter.writeNext(lines.get(1));
	        tempWriter.flush();
	        tempWriter.close();

	        lines.set(1, new String[] {tempStream.toString(encoding).substring(0, tempStream.toString(encoding).length() - 1)});
        
        	String[] recordToWrite = new String[4];
        	int i = 0;
        	for (String[] line : lines) {
        		for (String element : line) {
        			recordToWrite[i]=element;
        			i++;
        		}
        	}
        	
//            synchronized (writer) {
//                for (String[] line : lines) {
                    writer.writeNext(recordToWrite);
//                }
//            }
        }
    }
	
    public void writeToFile(List<String[]> lines, boolean unique)
            throws IOException {
        if (unique) {
            if (lines != null) {
                synchronized (writer) {
                    for (String[] line : lines) {
                        if (crosswalkUnique.add(line[0] + line[1])) {
                            writer.writeNext(line);
                        }
                    }
                }
            }

        } else {
            writeToFile(lines);
        }

    }
}
