package com.reltio.file;

import com.opencsv.CSVWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Mallikarjuna.Aakulati@reltio.com Created : July 05, 2021
 */
public class ReltioRejectFileWriter implements ReltioFileWriter, AutoCloseable {
    private static Logger logger = LoggerFactory.getLogger(ReltioRejectFileWriter.class.getName());


    private final CSVWriter cw;
    private final Set<String> crosswalkUnique = new HashSet<String>();
    private String encoding="UTF-8";
    CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();

    public ReltioRejectFileWriter(String fileName, String separator) throws IOException {
    	char csvSeparator;
        encoder.onMalformedInput(CodingErrorAction.REPLACE);
        encoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
        FileOutputStream os = new FileOutputStream(fileName);
        os.write(0xef);
        os.write(0xbb);
        os.write(0xbf);
        if (separator != null) {
        	csvSeparator=separator.charAt(0);
        } else {
        	csvSeparator = '|';
        }
        //cw = new CSVWriter(new OutputStreamWriter(os), csvSeparator);
        cw = new CSVWriter(new OutputStreamWriter(os, encoder), csvSeparator,
                CSVWriter.DEFAULT_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);
    }

    public ReltioRejectFileWriter(String fileName, String encoding, String separator)
            throws IOException {
    	char csvSeparator;
    	this.encoding=encoding;
        encoder = Charset.forName(encoding).newEncoder();
        encoder.onMalformedInput(CodingErrorAction.REPORT);
        encoder.onUnmappableCharacter(CodingErrorAction.REPORT);
        if (separator != null) {
        	csvSeparator=separator.charAt(0);
        } else {
        	csvSeparator = '|';
        }
//        cw = new CSVWriter(new OutputStreamWriter(
//                new FileOutputStream(fileName), encoder),csvSeparator);
        cw = new CSVWriter(new OutputStreamWriter(new FileOutputStream(fileName), encoder), csvSeparator,
                CSVWriter.DEFAULT_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);

    }

    public void writeToFile(List<String[]> lines) throws IOException {
    	if (lines != null) {
    		
    		ByteArrayOutputStream tempStream = new ByteArrayOutputStream();
	        OutputStreamWriter streamWriter = new OutputStreamWriter(tempStream, encoder.charset());
	        CSVWriter tempWriter = new CSVWriter(streamWriter, '|',
	                CSVWriter.DEFAULT_QUOTE_CHARACTER,
	                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
	                CSVWriter.DEFAULT_LINE_END);
	        tempWriter.writeNext(lines.get(1));
	        tempWriter.flush();
	        tempWriter.close();

	        lines.set(1, new String[] {tempStream.toString(encoding).substring(0, tempStream.toString(encoding).length() - 1)});
        
        	String[] recordToWrite = new String[4];
        	int i = 0;
        	for (String[] line : lines) {
        		for (String element : line) {
        			recordToWrite[i]=element;
        			i++;
        		}
        	}
            synchronized (cw) {
//                for (String[] line : lines) {
                    cw.writeNext(recordToWrite);
//                }
            }
        }
    }

    public void writeToFile(List<String[]> lines, boolean unique)
            throws IOException {
        if (unique) {
            if (lines != null) {
                synchronized (cw) {
                    for (String[] line : lines) {
                        if (crosswalkUnique.add(line[0] + line[1])) {
                            cw.writeNext(line);

                        }
                    }
                }
            }

        } else {
            writeToFile(lines);
        }

    }

    public void writeToFile(String[] line) {
        if (line != null) {
            synchronized (cw) {
                cw.writeNext(line);
            }
        }
    }

    public void close() throws IOException {
        synchronized (cw) {
            cw.close();
        }
    }

    @Override
    public void writeBulkToFile(List<String> lines) {
        // TODO Not implemented

    }

    @Override
    public void writeToFile(String line) {
        // TODO Not Implemented

    }
}
