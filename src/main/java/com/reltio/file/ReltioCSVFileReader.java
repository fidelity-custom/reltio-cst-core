package com.reltio.file;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.tika.parser.txt.CharsetDetector;
import org.apache.tika.parser.txt.CharsetMatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;

public class ReltioCSVFileReader implements ReltioFileReader, AutoCloseable {
    private static Logger logger = LoggerFactory.getLogger(ReltioCSVFileReader.class.getName());


    private final CSVReader csvReader;

    public ReltioCSVFileReader(String fileName) throws IOException {
        BufferedInputStream isr = new BufferedInputStream(new FileInputStream(
                StringEscapeUtils.escapeJava(fileName)));

        CharsetDetector charsetDetector = new CharsetDetector();
        charsetDetector.setText(isr);
        charsetDetector.enableInputFilter(true);
        CharsetMatch cm = charsetDetector.detect();
        logger.info("Decorder of the File (CharSet) :: " + cm.getName());
        isr.close();
        BufferedReader fileReader = null;
        if (Charset.availableCharsets().get(cm.getName()) == null) {
            logger.info("The "
                    + cm.getName()
                    + " charset not supported. So letting the reader to choose apporiate the Charset...");
            fileReader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(
                            StringEscapeUtils.escapeJava(fileName))));

        } else {
            CharsetDecoder newdecoder = Charset.forName(cm.getName())
                    .newDecoder();
            newdecoder.onMalformedInput(CodingErrorAction.REPLACE);
            fileReader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(StringEscapeUtils
                            .escapeJava(fileName)), newdecoder));

        }
        readBOMMarker(fileReader);
//        csvReader = new CSVReader(fileReader);
        CSVParser parser = new CSVParserBuilder().withSeparator(',').withEscapeChar(CSVWriter.NO_ESCAPE_CHARACTER).build();
        csvReader = new CSVReaderBuilder(fileReader).withCSVParser(parser).build();

    }

    public ReltioCSVFileReader(String fileName, String decoder)
            throws FileNotFoundException {
        CharsetDecoder newdecoder = Charset.forName(decoder).newDecoder();
        newdecoder.onMalformedInput(CodingErrorAction.REPLACE);
        BufferedReader fileReader = new BufferedReader(new InputStreamReader(
                new FileInputStream(StringEscapeUtils.escapeJava(fileName)),
                newdecoder));
        readBOMMarker(fileReader);
//        csvReader = new CSVReader(fileReader);
        CSVParser parser = new CSVParserBuilder().withSeparator(',').withEscapeChar(CSVWriter.NO_ESCAPE_CHARACTER).build();
        csvReader = new CSVReaderBuilder(fileReader).withCSVParser(parser).build();
    }

    @Override
    public String[] readLine() throws IOException {
    	String[] newLineValues = null;
        try {
        	newLineValues = csvReader.readNext();
		} catch (CsvValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return newLineValues;
    }

    @Override
    public void close() throws IOException {
        csvReader.close();
    }

    private void readBOMMarker(BufferedReader fileReader) {
        try {
            fileReader.mark(4);
            if ('\ufeff' != fileReader.read())
                fileReader.reset(); // not the BOM marker
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
