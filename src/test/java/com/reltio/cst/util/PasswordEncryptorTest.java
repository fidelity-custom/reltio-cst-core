package com.reltio.cst.util;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class PasswordEncryptorTest {

    @Test
    public void encryptPassword() {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword("Reltio");
        Assert.assertEquals("1234", encryptor.decrypt("g+8cpzVTLQfSdGecy8U0Vw\\=\\="));
    }

    @Test
    public void simpleTest() {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword("Reltio");
        Assert.assertEquals("vignesh",
                encryptor.decrypt("vD1Sp08DwtSBrm27EpKZGA\\=\\="));
    }


    @Test
    @Ignore
    public void encryptPasswordTest() {
        String encrypted = "A+cl20HJ1OuAMJBib9jeBA\\=\\=";
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword("Reltio");
        System.out.println(encryptor.decrypt(encrypted));
    }
}