package com.reltio.cst.service.impl;

import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.TokenGeneratorService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class SimpleReltioAPIServiceImplTest {

    private static final String tenantURL = "https://sndbx.reltio.com/reltio/api/yboOEM097UepwC3/entities/";
    private ReltioAPIService reltioAPIService;

    @Before
    public void setUp() throws Exception {

        TokenGeneratorService tokenGeneratorService = new TokenGeneratorServiceImpl(
                TokenGeneratorServiceImplTest.username,
                TokenGeneratorServiceImplTest.password,
                TokenGeneratorServiceImplTest.validAuthUrl);
        reltioAPIService = new SimpleReltioAPIServiceImpl(tokenGeneratorService);
    }


    @Test
    public void testGetString_Success() throws
            GenericException, ReltioAPICallFailureException {
        String response = reltioAPIService.get(tenantURL + "_total");
        System.out.println(response);
        Assert.assertNotNull(response);

    }

    @Test(expected = ReltioAPICallFailureException.class)
    public void testGetString_FailureInvalidURL()
            throws GenericException, ReltioAPICallFailureException {
        String response = reltioAPIService.get(tenantURL + "_totalsss");
        System.out.println(response);
        Assert.assertNotNull(response);

    }

    @Test(expected = GenericException.class)
    public void testGetString_FailureInvalidHostname()
            throws GenericException, ReltioAPICallFailureException {
        String response = reltioAPIService
                .get("https://devvvv.reltio.com/reltio/api/_totalsss");
        System.out.println(response);
        Assert.assertNotNull(response);
    }

    @Test
    public void testGetStringMapOfStringString_Sucesss()
            throws GenericException, ReltioAPICallFailureException {
        String response = reltioAPIService.get(tenantURL + "_total", null);
        System.out.println(response);
        Assert.assertNotNull(response);
    }

    @Test
    public void testPostStringString_Success() throws
            GenericException, ReltioAPICallFailureException {
        String response = reltioAPIService
                .post(tenantURL
                                + "_scan?filter=(equals(type,'configuration/entityTypes/Individual'))&select=uri&max=10",
                        null);
        Assert.assertNotNull(response);
    }


    @Test
    @Ignore
    public void testDeleteStringString_Success() throws
            GenericException, ReltioAPICallFailureException {
        String response = reltioAPIService
                .delete("https://362-dataload.reltio.com/reltio/api/KJErJRLl035LhOM/relations/3KN7vm7/crosswalks/HUphhX6J",
                        null);
        System.out.println(response);
        Assert.assertNotNull(response);
    }

    @Test
    @Ignore
    public void specialCharaterTest() throws GenericException, ReltioAPICallFailureException {


        String azID = "RU154200";
        String entityType = "HCA";

        String uri = "https://eu-360.reltio.com/reltio/api/HB6dGwbfIXVTOf3" + "/entities?filter=(equals(type, 'configuration/entityTypes/"
                + entityType + "') and equals(attributes.AZCustomerID,'" + azID + "'))&options=sendHidden&select=attributes ";
        String response = reltioAPIService
                .get(uri);
        System.out.println(response);

    }


    @Test
    @Ignore
    public void specialCharaterTestForAZ() throws GenericException, ReltioAPICallFailureException, UnsupportedEncodingException {

        String crosswalkValue = "111%";


        String encodedUrl = URLEncoder.encode(crosswalkValue, StandardCharsets.UTF_8.name());

        System.out.println(encodedUrl);
        String url = "https://sndbx.reltio.com/reltio/api/aO3zrh5wJaqmfGE/entities/qbbNOoZ/attributes/LastName/1Nd0CTP8j?crosswalkValue=" + encodedUrl;
        Map<String, String> header = new HashMap<>();
        header.put("Source-System", "configuration/sources/ACPA");
        System.out.println(reltioAPIService.delete(url, header, null));
    }


}
